# Teste Técnico: Rodrigo Fernandes

Solução para o [teste técnico](https://bitbucket.org/delivery_center/test-dev-backend/src/master/) para a vaga de Dev Backend realizado com a linguagem Go.

#### Pré-Requisitos
 - Instalar o [Go](https://golang.org/) no sistema. Caso não tenha, siga as [instruções do site oficial](https://golang.org/doc/install).

#### Rodar o Projeto
Execute a applicação com o comando abaixo
```bash
go run main.go
```
