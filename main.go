package main

import (
	"bytes"
	"delivery.app/serializers/input_serializer"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

func main() {
	println("Starting the app...\n")
	route := gin.Default()
	route.POST("/delivery", delivery)
	route.Run(":8085")
}

func delivery(c *gin.Context) {

	var orderFromRequest input_serializer.Order

	if c.ShouldBind(&orderFromRequest) == nil {

		data, err := orderFromRequest.ToOutputFormatAsJSON()

		if err != nil {
			print(err)
			c.String(400, "Bad Request")
		} else {
			response, err := http.Post("https://delivery-center-recruitment-ap.herokuapp.com/",
				"application/json", bytes.NewBuffer(data))

			defer response.Body.Close()
			responseBody, err := ioutil.ReadAll(response.Body)
			body := fmt.Sprintf("Service Response: %s", responseBody)

			fmt.Println(string(body))
			receivedData, err := orderFromRequest.AsJSON()
			fmt.Println(fmt.Sprintf("Received data:\n%s", receivedData))
			fmt.Println(fmt.Sprintf("Sent data:\n%s", data))

			if err != nil {
				print(err)
				c.String(400, "Bad Request")
			} else {
				c.String(200, string(body))
			}
		}
	} else {
		c.String(400, "Bad Request")
	}

}
