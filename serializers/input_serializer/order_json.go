package input_serializer

import (
	"delivery.app/serializers/output_serializer"
	"encoding/json"
	"fmt"
	"time"
)

type Order struct {
	Id                      int          `json:"id"`
	StoreId                 int          `json:"store_id"`
	DateCreated             time.Time    `json:"date_created"`
	DateClosed              string       `json:"date_closed"`
	LastUpdated             string       `json:"last_updated"`
	TotalAmount             float64      `json:"total_amount"`
	TotalShipping           float64      `json:"total_shipping"`
	TotalAmountWithShipping float64      `json:"total_amount_with_shipping"`
	PaidAmount              float64      `json:"paid_amount"`
	ExpirationDate          string       `json:"expiration_date"`
	OrderItems              []OrderItems `json:"order_items"`
	Payments                []Payments   `json:"payments"`
	Shipping                Shipping     `json:"shipping"`
	Status                  string       `json:"status"`
	Buyer                   Buyer        `json:"buyer"`
}

func toOutputFormat(order Order) output_serializer.Order {

	outputOrder := output_serializer.Order{
		ExternalCode:  order.Id,
		StoreId:       order.StoreId,
		SubTotal:      fmt.Sprint(order.TotalAmount),
		DeliveryFee:   fmt.Sprint(order.TotalShipping),
		Total:         fmt.Sprint(order.TotalAmount),
		Country:       order.Shipping.ReceiverAddress.Country.Name,
		State:         order.Shipping.ReceiverAddress.State.Name,
		City:          order.Shipping.ReceiverAddress.City.Name,
		District:      order.Shipping.ReceiverAddress.Neighborhood.Name,
		Street:        order.Shipping.ReceiverAddress.StreetName,
		Complement:    order.Shipping.ReceiverAddress.Comment,
		Latitude:      order.Shipping.ReceiverAddress.Latitude,
		Longitude:     order.Shipping.ReceiverAddress.Longitude,
		PostalCode:    order.Shipping.ReceiverAddress.ZipCode,
		Number:        order.Shipping.ReceiverAddress.StreetNumber,
		DtOrderCreate: order.DateCreated,
		Customer: output_serializer.Customer{
			ExternalCode: fmt.Sprint(order.Buyer.Id),
			Name:         order.Buyer.Nickname,
			Email:        order.Buyer.Email,
			Contact:      fmt.Sprintf("%v%v", order.Buyer.Phone.AreaCode, order.Buyer.Phone.Number),
		},
		Items:         []output_serializer.Items{},
		Payments:      []output_serializer.Payments{},
		TotalShipping: order.TotalShipping,
	}

	for _, orderItem := range order.OrderItems {
		outputOrder.Items = append(outputOrder.Items, output_serializer.Items{
			ExternalCode: orderItem.Item.Id,
			Name:         orderItem.Item.Title,
			Price:        orderItem.UnitPrice,
			Quantity:     orderItem.Quantity,
			Total:        orderItem.FullUnitPrice,
			SubItems:     []string{},
		})
	}

	for _, payment := range order.Payments {
		outputOrder.Payments = append(outputOrder.Payments, output_serializer.Payments{
			Type:  payment.PaymentType,
			Value: payment.TotalPaidAmount,
		})
	}
	return outputOrder
}

func (o Order) ToOutputFormatAsJSON() ([]byte, error) {
	outputOrder := toOutputFormat(o)
	data, err := json.Marshal(outputOrder)
	return data, err
}

func (o Order) AsJSON() ([]byte, error) {
	data, err := json.Marshal(o)
	return data, err
}