package input_serializer

type ReceiverAddress struct {
	Id            int     `json:"id"`
	AddressLine   string  `json:"address_line"`
	StreetName    string  `json:"street_name"`
	StreetNumber  string  `json:"street_number"`
	Comment       string  `json:"comment"`
	ZipCode       string  `json:"zip_code"`
	Latitude      float64 `json:"latitude"`
	Longitude     float64 `json:"longitude"`
	ReceiverPhone string  `json:"receiver_phone"`

	City struct {
		Name string `json:"name"`
	} `json:"city"`

	State struct {
		Name string `json:"name"`
	} `json:"state"`

	Country struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"country"`

	Neighborhood struct {
		ID   interface{} `json:"id"`
		Name string      `json:"name"`
	} `json:"neighborhood"`
}
