package input_serializer

type Payments struct {
	Id                int     `json:"id"`
	OrderId           int     `json:"order_id"`
	PayerId           int     `json:"payer_id"`
	Installments      int     `json:"installments"`
	PaymentType       string  `json:"payment_type"`
	Status            string  `json:"status"`
	TransactionAmount float64 `json:"transaction_amount"`
	TaxesAmount       int     `json:"taxes_amount"`
	ShippingCost      float64 `json:"shipping_cost"`
	TotalPaidAmount   float64 `json:"total_paid_amount"`
	InstallmentAmount float64 `json:"installment_amount"`
	DateApproved      string  `json:"date_approved"`
	DateCreated       string  `json:"date_created"`
}
