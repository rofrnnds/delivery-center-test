package input_serializer

type Buyer struct {
	Id        int    `json:"id"`
	Nickname  string `json:"nickname"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`

	Phone struct {
		AreaCode int    `json:"area_code"`
		Number   string `json:"number"`
	} `json:"phone"`

	BillingInfo struct {
		Doctype   string `json:"doc_type"`
		DocNumber string `json:"doc_number"`
	} `json:"billing_info"`
}
