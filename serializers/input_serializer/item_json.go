package input_serializer

type Item struct {
	Id    string `json:"id"`
	Title string `json:"title"`
}