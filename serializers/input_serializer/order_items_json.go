package input_serializer

type OrderItems struct {
	Quantity      int     `json:"quantity"`
	UnitPrice     float64 `json:"unit_price"`
	FullUnitPrice float64 `json:"full_unit_price"`
	Item          Item    `json:"item"`
}