package input_serializer

type Shipping struct {
	Id              int64           `json:"id"`
	ShipmentType    string          `json:"shipment_type"`
	DateCreated     string          `json:"date_created"`
	ReceiverAddress ReceiverAddress `json:"receiver_address"`
}
