package output_serializer

type Customer struct {
	ExternalCode string `json:"externalCode"`
	Name         string `json:"name"`
	Email        string `json:"email"`
	Contact      string `json:"contact"`
}