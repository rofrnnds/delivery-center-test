package output_serializer

import (
	"time"
)

type Order struct {
	ExternalCode  int        `json:"externalCode"`
	StoreId       int        `json:"storeId"`
	SubTotal      string     `json:"subTotal"`
	DeliveryFee   string     `json:"deliveryFee"`
	Total         string     `json:"total"`
	Country       string     `json:"country"`
	State         string     `json:"state"`
	City          string     `json:"city"`
	District      string     `json:"district"`
	Street        string     `json:"street"`
	Complement    string     `json:"complement"`
	Latitude      float64    `json:"latitude"`
	Longitude     float64    `json:"longitude"`
	DtOrderCreate time.Time  `json:"dtOrderCreate"`
	PostalCode    string     `json:"postalCode"`
	Number        string     `json:"number"`
	TotalShipping float64    `json:"total_shipping"`
	Customer      Customer   `json:"customer"`
	Items         []Items    `json:"items"`
	Payments      []Payments `json:"payments"`
}
