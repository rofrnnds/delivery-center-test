package output_serializer

type Items struct {
	ExternalCode string   `json:"externalCode"`
	Name         string   `json:"name"`
	Price        float64  `json:"price"`
	Quantity     int      `json:"quantity"`
	Total        float64  `json:"total"`
	SubItems     []string `json:"subItems"`
}