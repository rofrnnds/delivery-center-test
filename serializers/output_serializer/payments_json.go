package output_serializer

type Payments struct {
	Type  string  `json:"type"`
	Value float64 `json:"value"`
}
